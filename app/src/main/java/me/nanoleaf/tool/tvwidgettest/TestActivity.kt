package me.nanoleaf.tool.tvwidgettest

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.TextView
import me.nanoleaf.plug.panel.NanoleafPanels
import me.nanoleaf.plug.panel.api.NanoleafOpenApi
import me.nanoleaf.plug.panel.view.PanelsView

class TestActivity : Activity() {
    private val handle = Handler(Looper.myLooper()!!)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var panelsView: PanelsView = findViewById(R.id.panel)
        Thread {
            try {
                var authorization = NanoleafOpenApi.authorization("10.0.0.141", 16021)
                var allControllerInfo = NanoleafOpenApi.getAllControllerInfo("10.0.0.141", 16021, authorization!!)
                var panels = NanoleafPanels(allControllerInfo!!, this)
                handle.post {
                    panelsView.setPanelsBitmap(panels.bitmap)
                }
            } catch (e: Exception) {
            }
        }.start()

    }
}