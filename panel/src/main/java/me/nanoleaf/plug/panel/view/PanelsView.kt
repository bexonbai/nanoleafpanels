package me.nanoleaf.plug.panel.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup

class PanelsView : View {
    private var bitmap: Bitmap? = null
    private var paint: Paint = Paint()
    private val canvasMatrix: Matrix = Matrix()
    private var scaleSize: Float = 1f
    init {
        paint.color = Color.RED
        paint.strokeWidth = 5f
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
    }
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    /**
     * Set the bitmap of the light board, the bitmap can be obtained from [me.nanoleaf.plug.panel.NanoleafPanels].
     */
    fun setPanelsBitmap(bitmap: Bitmap?) {
        this.bitmap = bitmap
        if (bitmap != null) {
            if (width > height) {
                scaleSize = width.toFloat() / bitmap.width
                scaleSize = if (bitmap.width > bitmap.height)
                    width.toFloat() / bitmap.width
                else
                    height.toFloat() / bitmap.height
            } else {
                scaleSize = height.toFloat() / bitmap.height
                scaleSize = if (bitmap.width > bitmap.height)
                    height.toFloat() / bitmap.height
                else
                    width.toFloat() / bitmap.width
            }
        }
        invalidate()
        forceLayout()
        requestLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        var mWidth = 0
        var mHeight = 0
        if (bitmap != null) {
            mWidth = bitmap!!.width
            mHeight = bitmap!!.height
        }

        if (layoutParams.width == ViewGroup.LayoutParams.WRAP_CONTENT && layoutParams.height == ViewGroup.LayoutParams.WRAP_CONTENT) {
            setMeasuredDimension(mWidth, mHeight)
        } else if (layoutParams.width == ViewGroup.LayoutParams.WRAP_CONTENT) {
            setMeasuredDimension(mWidth, heightSize)
        } else if (layoutParams.height == ViewGroup.LayoutParams.WRAP_CONTENT) {
            setMeasuredDimension(widthSize, mHeight)
        }
        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas != null && bitmap != null) {
            if (bitmap!!.width > 0 && bitmap!!.height > 0) {
                canvasMatrix.postScale(scaleSize, scaleSize)
                canvasMatrix.postTranslate((width - bitmap!!.width * scaleSize) / 2f, (height - bitmap!!.height * scaleSize) / 2f)
                canvas.drawBitmap(bitmap!!, canvasMatrix, paint)
            }
        }
    }
}