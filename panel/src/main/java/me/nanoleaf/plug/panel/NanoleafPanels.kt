/*
 * Copyright (C) 2021 Nanoleaf Ltd, All rights reserved
 *
 * Modification and distribution are prohibited without permission.
 * https://nanoleaf.me
 */
package me.nanoleaf.plug.panel

import android.content.Context
import android.graphics.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import me.nanoleaf.plug.panel.api.NanoleafOpenApi
import me.nanoleaf.plug.panel.model.Data
import me.nanoleaf.plug.panel.model.Panels
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.CONTROL_SQUARE_MASTER
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.CONTROL_SQUARE_PASSIVE
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.HEXAGON_SHAPES
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.HEXAGON_SHAPES_SIDE_LENGTH
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.MINI_TRIANGLE_SHAPES
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.MINI_TRIANGLE_SHAPES_SIDE_LENGTH
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.SQUARE
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.SQUARE_SIDE_LENGTH
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.TRIANGLE
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.TRIANGLE_SHAPES
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.TRIANGLE_SHAPES_SIDE_LENGTH
import me.nanoleaf.plug.panel.model.PanelsParameters.Companion.TRIANGLE_SIDE_LENGTH
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.ceil
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

/**
 * Nanoleaf panels
 */
class NanoleafPanels(json: String, private var context: Context){

    /** Panel's all controller info. */
    var panels : Panels

    private var originalCenter: Coordinate? = null
    private var coordinatesAfterRotateAndScale: Array<Coordinate?>
    private var originalVertexes: Array<Array<Coordinate?>?>
    private var vertexesAfterRotateAndScale: Array<Array<Coordinate?>?>
    private var safeArea: Coordinate
    var vertexes: Array<Array<Coordinate?>?>
        private set
    var coordinates: Array<Coordinate?>
        private set
    var center: Coordinate
        private set
    var layout: Array<Data?>
        private set
    private var widthHeight: FloatArray
    var bitmap: Bitmap?
        private set

    init {
        panels = parser(json)
        originalCenter = panels.panelLayout?.layout?.positionData?.let { getCenter(it) }
        coordinatesAfterRotateAndScale = coordinatesAfterRotateAndScale(panels)
        originalVertexes = getVertex(panels)
        vertexesAfterRotateAndScale = vertexesAfterRotateAndScale(panels, originalVertexes)
        safeArea = getSaveArea(vertexesAfterRotateAndScale)
        vertexes = getVertexesForDisplay(vertexesAfterRotateAndScale, safeArea)
        coordinates = getCoordinatesForDisplay(coordinatesAfterRotateAndScale, safeArea)
        center = getCenterForDisplay(originalCenter, safeArea)
        layout = getLayout(panels, coordinates)
        widthHeight = getWidthHeight(vertexesAfterRotateAndScale)
        bitmap = getBitmap(layout)
    }

    /**
     * return FloatArray[width, height]
     */
    private fun getWidthHeight(vertexes: Array<Array<Coordinate?>?>): FloatArray {
        val xList: MutableList<Float> = ArrayList()
        val yList: MutableList<Float> = ArrayList()
        vertexes.forEach {
            it?.forEach { c ->
                c?.x?.let { x -> xList.add(x) }
                c?.y?.let { y -> yList.add(y) }
            }
        }
        var minx = 0f
        var miny = 0f
        var maxx = 0f
        var maxy = 0f

        if (xList.size > 0 && yList.size > 0) {
            minx = Collections.min(xList)
            miny = Collections.min(yList)
            maxx = Collections.max(xList)
            maxy = Collections.max(yList)
        }
        val wh = FloatArray(2)
        wh[0] = maxx - minx
        wh[1] = maxy - miny
        return wh
    }

    /**
     * Get coordinates for display(in safe-area)
     */
    private fun getCoordinatesForDisplay(coordinates: Array<Coordinate?>, safeArea: Coordinate): Array<Coordinate?> {
        val list: Array<Coordinate?> = arrayOfNulls(coordinates.size)
        coordinates.forEachIndexed { index, coordinate ->
            if (coordinate != null) {
                val c = Coordinate(
                    x = coordinate.x + safeArea.x,
                    y = coordinate.y + safeArea.y
                )
                list[index] = c
            }
        }
        return list
    }

    /**
     * Get vertexs for display(in safe-area)
     */
    private fun getVertexesForDisplay(vertexes: Array<Array<Coordinate?>?>, safeArea: Coordinate): Array<Array<Coordinate?>?> {
        val v: Array<Array<Coordinate?>?> = arrayOfNulls(vertexes.size)
        vertexes.forEachIndexed { index, arrayOfCoordinates ->
            if (arrayOfCoordinates != null) {
                val array: Array<Coordinate?> = arrayOfNulls(arrayOfCoordinates.size)
                arrayOfCoordinates.forEachIndexed { i, coordinate ->
                    if (coordinate != null) {
                        val c = Coordinate(
                            x = coordinate.x + safeArea.x,
                            y = coordinate.y + safeArea.y
                        )
                        array[i] = c
                    }
                }
                v[index] = array
            }
        }
        return v
    }

    /**
     * Get [vertexes]'s safe-area margin.
     */
    private fun getSaveArea(vertexes: Array<Array<Coordinate?>?>): Coordinate {
        val xList: MutableList<Float> = ArrayList()
        val yList: MutableList<Float> = ArrayList()
        vertexes.forEach {
            it?.forEach { c ->
                c?.x?.let { x -> xList.add(x) }
                c?.y?.let { y -> yList.add(y) }
            }
        }
        var minx = 0f
        var miny = 0f
        if (xList.size > 0 && yList.size > 0) {
            minx = Collections.min(xList)
            miny = Collections.min(yList)

        }
        return Coordinate(-minx, -miny)
    }

    /**
     * Parser panel's JSON to [Panels].
     */
    private fun parser(json: String): Panels {
        val format = Json { ignoreUnknownKeys = true }
        return format.decodeFromString(json)
    }

    /**
     * Get vertexs after rotate and scale.
     */
    private fun vertexesAfterRotateAndScale(panels: Panels, vertexs: Array<Array<Coordinate?>?>): Array<Array<Coordinate?>?> {
        if (panels.panelLayout?.layout?.positionData != null && panels.panelLayout?.globalOrientation != null) {
            val size = vertexs.size
            val globalOrientation = -panels.panelLayout?.globalOrientation!!.value
            val vertexAfterRotate: Array<Array<Coordinate?>?> = arrayOfNulls(size)
            vertexs.forEachIndexed { index, data ->
                val v: Array<Coordinate?> = arrayOfNulls<Coordinate?>(data?.size ?: 0)
                data?.forEachIndexed { i, it ->
                    if (it != null && originalCenter != null) {
                        val center: Coordinate = originalCenter!!
                        val coordinate = Coordinate(
                            x = ((it.x - center.x) * cos(Math.toRadians(globalOrientation.toDouble())) - (it.y - center.y) * sin(Math.toRadians(globalOrientation.toDouble())) + center.x).toFloat(),
                            y = ((it.y - center.y) * cos(Math.toRadians(globalOrientation.toDouble())) + (it.x - center.x) * sin(Math.toRadians(globalOrientation.toDouble())) + center.y).toFloat()
                        )
                        v[i] = coordinate
                    }
                    vertexAfterRotate[index] = v
                }
            }
            val vertexAfterScale: Array<Array<Coordinate?>?> = arrayOfNulls(size)
            vertexAfterRotate.forEachIndexed { index, data ->
                val v: Array<Coordinate?> = arrayOfNulls<Coordinate?>(data?.size ?: 0)
                data?.forEachIndexed { i, it ->
                    if (it != null && originalCenter != null) {
                        val coordinate = Coordinate(
                            x = it.x,
                            y = it.y - 2 * (it.y - originalCenter!!.y)
                        )
                        v[i] = coordinate
                    }
                    vertexAfterScale[index] = v
                }
            }
            return vertexAfterScale
        } else {
            return emptyArray()
        }
    }

    /**
     * Get orginal coordinate.
     */
    private fun coordinatesAfterRotateAndScale(panels: Panels): Array<Coordinate?> {
        if (panels.panelLayout?.layout?.positionData != null && panels.panelLayout?.globalOrientation != null) {
            val positionData = panels.panelLayout?.layout?.positionData!!
            val globalOrientation = -panels.panelLayout?.globalOrientation!!.value
            val size = panels.panelLayout?.layout?.positionData!!.size
            val center: Coordinate = originalCenter!!
            val afterRotate: Array<Coordinate?> = arrayOfNulls(size)
            positionData.forEachIndexed { index, it ->
                val coordinate = Coordinate(
                    x = ((it.x - center.x) * cos(Math.toRadians(globalOrientation.toDouble())) - (it.y - center.y) * sin(Math.toRadians(globalOrientation.toDouble())) + center.x).toFloat(),
                    y = ((it.y - center.y) * cos(Math.toRadians(globalOrientation.toDouble())) + (it.x - center.x) * sin(Math.toRadians(globalOrientation.toDouble())) + center.y).toFloat()
                )
                afterRotate[index] = coordinate
            }
            val afterScale: Array<Coordinate?> = arrayOfNulls(size)
            afterRotate.forEachIndexed { index, data ->
                val coordinate = Coordinate(
                    x = data!!.x,
                    y = data.y - 2 * (data.y - center.y)
                )
                afterScale[index] = coordinate
            }
            return afterScale
        } else {
            return emptyArray()
        }
    }

    /**
     * Get center for display.(in safe-area)
     */
    private fun getCenterForDisplay(originalCenter: Coordinate?, safeArea: Coordinate): Coordinate {
        return if (originalCenter != null) {
            Coordinate(
                x = originalCenter.x + safeArea.x,
                y = originalCenter.y + safeArea.y
            )
        } else {
            Coordinate(0f, 0f)
        }
    }

    /**
     * Get panels' layout datas
     */
    private fun getLayout(panels: Panels, coordinates: Array<Coordinate?>): Array<Data?> {
        val array = arrayOfNulls<Data>(coordinates.size)
        val globalOrientation = panels.panelLayout?.globalOrientation?.value
        coordinates.forEachIndexed { index, coordinate ->
            val o = panels.panelLayout?.layout?.positionData?.get(index)?.o
            val panelId = panels.panelLayout?.layout?.positionData?.get(index)?.panelId
            val shapeType = panels.panelLayout?.layout?.positionData?.get(index)?.shapeType
            if (coordinate != null && o != null && globalOrientation != null && panelId != null && shapeType != null) {
                val data = Data(
                    panelId = panelId,
                    shapeType = shapeType,
                    x = coordinate.x,
                    y = coordinate.y,
                    o = o + (globalOrientation)
                )
                array[index] = data
            }
        }
        return array
    }

    /**
     * get orginal vertex.
     */
    private fun getVertex(panels: Panels): Array<Array<Coordinate?>?> {
        if (panels.panelLayout?.layout?.positionData != null && panels.panelLayout?.globalOrientation != null) {
            val size = panels.panelLayout?.layout?.positionData!!.size
            val positionData = panels.panelLayout?.layout?.positionData!!
            val panelsVertex: Array<Array<Coordinate?>?> = arrayOfNulls(size)
            positionData.forEachIndexed { index, data ->
                val vertexes: Array<Coordinate?>
                val x = data.x
                val y = data.y
                val o = data.o
                if (data.shapeType == TRIANGLE || data.shapeType == TRIANGLE_SHAPES || data.shapeType == MINI_TRIANGLE_SHAPES) {
                    vertexes = arrayOfNulls<Coordinate?>(3)
                    var halfSideLength = TRIANGLE_SIDE_LENGTH / 2f
                    var rInnerCircle = halfSideLength / sqrt(3.0).toFloat()
                    var rOuterCircle = TRIANGLE_SIDE_LENGTH / sqrt(3.0).toFloat()
                    if (data.shapeType == TRIANGLE_SHAPES) {
                        halfSideLength = TRIANGLE_SHAPES_SIDE_LENGTH / 2f
                        rInnerCircle = halfSideLength / sqrt(3.0).toFloat()
                        rOuterCircle = TRIANGLE_SHAPES_SIDE_LENGTH / sqrt(3.0).toFloat()
                    } else if (data.shapeType == MINI_TRIANGLE_SHAPES) {
                        halfSideLength = MINI_TRIANGLE_SHAPES_SIDE_LENGTH / 2f
                        rInnerCircle = halfSideLength / sqrt(3.0).toFloat()
                        rOuterCircle = MINI_TRIANGLE_SHAPES_SIDE_LENGTH / sqrt(3.0).toFloat()
                    }
                    if (o % 120 == 0) {
                        vertexes[0] = Coordinate(x = x - halfSideLength, y = y - rInnerCircle)
                        vertexes[1] = Coordinate(x = x, y = y + rOuterCircle)
                        vertexes[2] = Coordinate(x = x + halfSideLength, y = y - rInnerCircle)
                    } else {
                        vertexes[0] = Coordinate(x = x - halfSideLength, y = y + rInnerCircle)
                        vertexes[1] = Coordinate(x = x, y = y - rOuterCircle)
                        vertexes[2] = Coordinate(x = x + halfSideLength, y = y + rInnerCircle)
                    }
                    panelsVertex[index] = vertexes
                } else if (data.shapeType == SQUARE || data.shapeType == CONTROL_SQUARE_MASTER || data.shapeType == CONTROL_SQUARE_PASSIVE) {
                    vertexes = arrayOfNulls<Coordinate?>(4)
                    vertexes[0] = Coordinate(x = x - SQUARE_SIDE_LENGTH / 2f, y = y + SQUARE_SIDE_LENGTH / 2f)
                    vertexes[1] = Coordinate(x = x + SQUARE_SIDE_LENGTH / 2f, y = y + SQUARE_SIDE_LENGTH / 2f)
                    vertexes[2] = Coordinate(x = x + SQUARE_SIDE_LENGTH / 2f, y = y - SQUARE_SIDE_LENGTH / 2f)
                    vertexes[3] = Coordinate(x = x - SQUARE_SIDE_LENGTH / 2f, y = y - SQUARE_SIDE_LENGTH / 2f)
                    panelsVertex[index] = vertexes
                } else if (data.shapeType == HEXAGON_SHAPES) {
                    vertexes = arrayOfNulls<Coordinate?>(6)
                    vertexes[0] = Coordinate(x = (x + HEXAGON_SHAPES_SIDE_LENGTH * cos(Math.toRadians(0.0))).toFloat(), y = (y + HEXAGON_SHAPES_SIDE_LENGTH * sin(Math.toRadians(0.0))).toFloat())
                    vertexes[1] = Coordinate(x = (x + HEXAGON_SHAPES_SIDE_LENGTH * cos(Math.toRadians(60.0))).toFloat(), y = (y + HEXAGON_SHAPES_SIDE_LENGTH * sin(Math.toRadians(60.0))).toFloat())
                    vertexes[2] = Coordinate(x = (x + HEXAGON_SHAPES_SIDE_LENGTH * cos(Math.toRadians(120.0))).toFloat(), y = (y + HEXAGON_SHAPES_SIDE_LENGTH * sin(Math.toRadians(120.0))).toFloat())
                    vertexes[3] = Coordinate(x = (x + HEXAGON_SHAPES_SIDE_LENGTH * cos(Math.toRadians(180.0))).toFloat(), y = (y + HEXAGON_SHAPES_SIDE_LENGTH * sin(Math.toRadians(180.0))).toFloat())
                    vertexes[4] = Coordinate(x = (x + HEXAGON_SHAPES_SIDE_LENGTH * cos(Math.toRadians(240.0))).toFloat(), y = (y + HEXAGON_SHAPES_SIDE_LENGTH * sin(Math.toRadians(240.0))).toFloat())
                    vertexes[5] = Coordinate(x = (x + HEXAGON_SHAPES_SIDE_LENGTH * cos(Math.toRadians(300.0))).toFloat(), y = (y + HEXAGON_SHAPES_SIDE_LENGTH * sin(Math.toRadians(300.0))).toFloat())
                    panelsVertex[index] = vertexes
                } else {
                    panelsVertex[index] = emptyArray()
                }
            }
            return panelsVertex
        } else {
            return emptyArray()
        }
    }

    /**
     * Get position center.
     */
    private fun getCenter(points: Array<Data>): Coordinate {
        val coordinate = Coordinate(x = 0F, y = 0F)
        points.forEach { i ->
            coordinate.x += i.x
            coordinate.y += i.y
        }
        coordinate.x /= points.size
        coordinate.y /= points.size
        return coordinate
    }

    /**
     * Get panels bitmap.
     */
    private fun getBitmap(layout: Array<Data?>): Bitmap? {
        val paint = Paint()
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        paint.color = Color.RED
        paint.strokeWidth = 5f
        val width = ceil(widthHeight[0].toDouble()).toInt()
        val height = ceil(widthHeight[1].toDouble()).toInt()
        if (width == 0 || height == 0) {
            return null
        }
        val mainBitmap = Bitmap.createBitmap(width, height + 15, Bitmap.Config.ARGB_8888)
        val mainCanvas = Canvas(mainBitmap)
        mainCanvas.drawFilter = PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)
        layout.forEach { data ->
            if (data != null) {
                var centroidX = 0f
                var centroidY = 0f
                var bitmap: Bitmap?
                when (data.shapeType) {
                    TRIANGLE -> {
                        centroidX = -146 / 2f
                        centroidY = -146 / 2f
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.aurora_down)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 146, 146, true)
                    }
                    SQUARE -> {
                        centroidX = -96 / 2f
                        centroidY = -96 / 2f
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.canvas_panel)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 96, 96, true)
                    }
                    CONTROL_SQUARE_MASTER -> {
                        centroidX = -96 / 2f
                        centroidY = -96 / 2f
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.canvas_controller)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 96, 96, true)
                    }
                    CONTROL_SQUARE_PASSIVE -> {
                        centroidX = -96 / 2f
                        centroidY = -96 / 2f
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.canvas_controller)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 96, 96, true)
                    }
                    HEXAGON_SHAPES -> {
                        centroidX = -67f
                        centroidY = (-sqrt(3.0) / 2 * 67).toFloat()
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.shapes_hexagon)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 2 * 67, (sqrt(3.0) * 67).toInt(), true)
                    }
                    TRIANGLE_SHAPES -> {
                        centroidX = -134 / 2f
                        centroidY = (-sqrt(3.0) / 6 * 134).toFloat()
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.shapes_triangle)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 134, (sin(Math.toRadians(60.0)) * 134).toInt(), true)
                    }
                    MINI_TRIANGLE_SHAPES -> {
                        centroidX = -67 / 2f
                        centroidY = (-sqrt(3.0) / 6 * 67).toFloat()
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.shapes_mini_triangle)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 67, (sin(Math.toRadians(60.0)) * 67).toInt(), true)
                    }
                    else -> {
                        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.unsupport_device)
                        bitmap = Bitmap.createScaledBitmap(bitmap, 130, 130, true)
                    }
                }
                val panelsPicture = Picture()
                val canvas = panelsPicture.beginRecording(width, height)
                canvas.drawFilter = PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)
                val panelPicture = Picture()
                val c = panelPicture.beginRecording(bitmap?.width!!, bitmap.height)
                c.drawFilter = PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)
                val matrix = Matrix()
                c.translate(data.x + centroidX, data.y + centroidY)
                c.rotate(data.o.toFloat() - 180, -centroidX, -centroidY)
                c.drawBitmap(bitmap, matrix, paint)
                panelPicture.endRecording()
                canvas.drawPicture(panelPicture)
                mainCanvas.drawPicture(panelsPicture)
            }
        }
        return mainBitmap
    }

    @Serializable data class Coordinate(var x: Float, var y: Float)
}