/*
 * Copyright (C) 2021 Nanoleaf Ltd, All rights reserved
 *
 * Modification and distribution are prohibited without permission.
 * https://nanoleaf.me
 */
package me.nanoleaf.plug.panel.model

import kotlin.math.sin
import kotlin.math.sqrt

/**
 * Panel's parameters.
 */
class PanelsParameters {
    companion object {
        const val TRIANGLE = 0
        const val TRIANGLE_SIDE_LENGTH = 150
        @JvmField val TRIANGLE_HEIGHT = (sin(Math.toRadians(60.0)) * TRIANGLE_SIDE_LENGTH).toInt()
        const val RHYTHM = 1
        const val SQUARE = 2
        const val SQUARE_SIDE_LENGTH = 100
        const val CONTROL_SQUARE_MASTER = 3
        const val CONTROL_SQUARE_MASTER_SIDE_LENGTH = 100
        const val CONTROL_SQUARE_PASSIVE = 4
        const val CONTROL_SQUARE_PASSIVE_SIDE_LENGTH = 100
        const val HEXAGON_SHAPES = 7
        const val HEXAGON_SHAPES_SIDE_LENGTH = 67
        @JvmField val HEXAGON_SHAPES_HEIGHT = (sqrt(3.0) * HEXAGON_SHAPES_SIDE_LENGTH).toInt()
        const val HEXAGON_SHAPES_WIDTH = 2 * HEXAGON_SHAPES_SIDE_LENGTH
        const val TRIANGLE_SHAPES = 8
        const val TRIANGLE_SHAPES_SIDE_LENGTH = 134
        @JvmField val TRIANGLE_SHAPES_HEIGHT = (sin(Math.toRadians(60.0)) * TRIANGLE_SHAPES_SIDE_LENGTH).toInt()
        const val MINI_TRIANGLE_SHAPES = 9
        const val MINI_TRIANGLE_SHAPES_SIDE_LENGTH = 67
        @JvmField val MINI_TRIANGLE_SHAPES_HEIGHT = (sin(Math.toRadians(60.0)) * MINI_TRIANGLE_SHAPES_SIDE_LENGTH).toInt()
        const val SHAPES_CONTROLLER = 12
    }
}