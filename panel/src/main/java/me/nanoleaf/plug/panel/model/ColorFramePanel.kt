/*
 * Copyright (C) 2021 Nanoleaf Ltd, All rights reserved
 *
 * Modification and distribution are prohibited without permission.
 * https://nanoleaf.me
 */
package me.nanoleaf.plug.panel.model

import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

class ColorFramePanel {
    var panelID = 0
    var colorR = 0
    var colorG = 0
    var colorB = 0
    var transTime = 0
    private var centroidX = 0.0
    private var centroidY = 0.0
    private var oV = 0

    private val mSideLengthForDisplay = 0
    private var centroidXForDisplay = 0.0
    private var centroidYForDisplay = 0.0
    private var shapeType = 0

    private lateinit var pixelRange: IntArray

    fun ColorFramePanel(_panelID: Int, shapeType: Int, sideNumber: Int, x: Int, y: Int, o: Int, sideLength: Int) {
        this.ColorFramePanel(_panelID, 0, 0, 0, 0, sideNumber, x, y, o, sideLength, shapeType)
    }

    fun ColorFramePanel(_panelID: Int, _colorR: Int, _colorG: Int, _colorB: Int, _transTime: Int, sideNumber: Int, x: Int, y: Int, o: Int, sideLength: Int, shapeType: Int) {
        panelID = _panelID
        colorB = _colorB
        colorG = _colorG
        colorR = _colorR
        transTime = _transTime
        centroidY = y.toDouble()
        centroidX = x.toDouble()
        oV = o
        this.shapeType = shapeType
    }

    private fun offsetXY(xOffset: Int, yOffset: Int) {
        centroidX += xOffset.toDouble()
        centroidY += yOffset.toDouble()
        centroidXForDisplay = centroidX
        centroidYForDisplay = centroidY
    }

    private fun convertXY(reverseX: Boolean, reverseY: Boolean, angle: Int, xOffset: Int, yOffset: Int, scale: Double, width: Int, height: Int) {
        val oldX: Double
        val oldY: Double
        val radian = Math.toRadians(angle.toDouble())
        val reverseXFactor = if (reverseX) -1 else 1
        val reverseYFactor = if (reverseY) -1 else 1
        oldX = centroidX * reverseXFactor
        oldY = centroidY * reverseYFactor
        centroidXForDisplay = (cos(radian) * oldX - sin(radian) * oldY) * scale + xOffset
        centroidYForDisplay = (cos(radian) * oldY + sin(radian) * oldX) * scale + yOffset
        pixelRange = getPixelRangeXYWH(width, height)
    }

    private fun getPixelRangeXYWH(width: Int, height: Int): IntArray {
        val r = mSideLengthForDisplay.toDouble()
        val sideLengthOfThePixelRange = sqrt(2.0) * r
        val xOfThePixelRange = centroidXForDisplay - sideLengthOfThePixelRange / 2
        val yOfThePixelRange = centroidYForDisplay - sideLengthOfThePixelRange / 2
        var x = xOfThePixelRange.toInt()
        var y = yOfThePixelRange.toInt()
        var w = sideLengthOfThePixelRange.toInt()
        var h = sideLengthOfThePixelRange.toInt()
        if (x < 0) {
            w += x
            x = 0
        }
        if (y < 0) {
            h += y
            y = 0
        }
        if (x + w > width) {
            w -= x + w - width
        }
        if (y + h > height) {
            h -= y + h - height
        }
        if (w < 0) w = 0
        if (h < 0) h = 0
        return intArrayOf(x, y, w, h, sideLengthOfThePixelRange.toInt())
    }
}
