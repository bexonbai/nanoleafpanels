/*
 * Copyright (C) 2021 Nanoleaf Ltd, All rights reserved
 *
 * Modification and distribution are prohibited without permission.
 * https://nanoleaf.me
 */
package me.nanoleaf.plug.panel.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable data class Panels(
    @SerialName("name") var name: String? = null, @SerialName("serialNo") var serialNo: String? = null, @SerialName("manufacturer") var manufacturer: String? = null,
    @SerialName("firmwareVersion") var firmwareVersion: String? = null,
    @SerialName("hardwareVersion") var hardwareVersion: String? = null, @SerialName("model") var model: String? = null, @SerialName("effects") var effects: Effects? = null,
    @SerialName("panelLayout") var panelLayout: PanelLayout? = null, @SerialName("rhythm") var rhythm: Rhythm? = null, @SerialName("state") var state: State? = null
)

@Serializable data class Effects(var effectsList: Array<String>? = null, var select: String? = null) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Effects

        if (!effectsList.contentEquals(other.effectsList)) return false
        if (select != other.select) return false

        return true
    }

    override fun hashCode(): Int {
        var result = effectsList.contentHashCode()
        result = 31 * result + select.hashCode()
        return result
    }
}

@Serializable data class PanelLayout(var globalOrientation: GlobalOrientation? = null, var layout: Layout? = null)

@Serializable data class GlobalOrientation(var value: Int, var min: Int, var max: Int)

@Serializable data class Layout(var numPanels: Int, var sideLength: Int, var positionData: Array<Data>?) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Layout

        if (numPanels != other.numPanels) return false
        if (sideLength != other.sideLength) return false
        if (!positionData.contentEquals(other.positionData)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = numPanels
        result = 31 * result + sideLength
        result = 31 * result + positionData.contentHashCode()
        return result
    }
}

@Serializable data class Data(var panelId: Int, var x: Float, var y: Float, var o: Int, var shapeType: Int)

@Serializable data class Rhythm(
    var rhythmConnected: Boolean? = null, var rhythmActive: Boolean? = null, var rhythmId: Int? = null, var hardwareVersion: String? = null,
    var firmwareVersion: String? = null, var auxAvailable: String? = null, var rhythmMode: Int? = null, var rhythmPos: RhythmPos? = null
)

@Serializable data class RhythmPos(var x: Int, var y: Int, var o: Int)

@Serializable data class State(var brightness: Brightness? = null, var colorMode: String? = null, var ct: CT? = null, var hue: Hue? = null, var on: ON? = null, var sat: Sat? = null)

@Serializable data class Brightness(var value: Int, var min: Int, var max: Int)

@Serializable data class CT(var value: Int, var min: Int, var max: Int)

@Serializable data class Hue(var value: Int, var min: Int, var max: Int)

@Serializable data class ON(var value: Boolean)

@Serializable data class Sat(var value: Int, var min: Int, var max: Int)