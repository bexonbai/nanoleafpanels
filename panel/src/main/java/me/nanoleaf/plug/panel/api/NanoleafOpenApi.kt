/*
 * Copyright (C) 2021 Nanoleaf Ltd, All rights reserved
 *
 * Modification and distribution are prohibited without permission.
 * https://nanoleaf.me
 */
package me.nanoleaf.plug.panel.api

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import me.nanoleaf.plug.panel.model.ColorFramePanel
import java.io.BufferedReader
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStreamReader
import java.net.*
import java.nio.charset.StandardCharsets
import java.util.*

class NanoleafOpenApi {
    companion object {
        private const val HTTP_URL = "http://%s:%d/api/v1/%s/%s"
        private val TAG = NanoleafOpenApi::class.java.name

        var udpSocket = DatagramSocket()

        private fun HttpRequest(ipAddr: String, port: Int, token: String, method: String, body: String): String? {
            return HttpRequest(ipAddr, port, token, "", method, body)
        }

        private fun HttpRequest(ipAddr: String?, port: Int, token: String, endpoint: String, method: String, body: String): String? {
            if (ipAddr == null || ipAddr === "") {
                return null
            }
            val requestURL = String.format(Locale.getDefault(), HTTP_URL, ipAddr, port, token, endpoint)
            return try {
                val url = URL(requestURL)
                val urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.requestMethod = method
                if (body.isNotEmpty()) {
                    urlConnection.setRequestProperty("Content-Type", "application/json; utf-8")
                    urlConnection.setRequestProperty("Accept", "application/json")
                    urlConnection.doOutput = true
                    val outputStream = urlConnection.outputStream
                    outputStream.write(body.toByteArray(StandardCharsets.UTF_8))
                    outputStream.flush()
                    outputStream.close()
                }
                var respCode = 0

                try {
                    respCode = urlConnection.responseCode
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (respCode == 200 || respCode == 204) {
                    val reader = BufferedReader(InputStreamReader(urlConnection.inputStream))
                    val result = StringBuilder()
                    var line: String?
                    while (reader.readLine().also { line = it } != null) {
                        result.append(line)
                    }
                    result.toString()
                } else {
                    null
                }
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }
        }

        @JvmStatic fun authorization(ipAddr: String?, port: Int): String? {
            if (ipAddr == null || ipAddr === "") {
                return null
            }
            var result: String? = null
            try {
                result = HttpRequest(ipAddr, port, "new", "", "POST", "")
            } catch (e: Exception) {
                e.printStackTrace();
            }
            return if (result != null) {
                val format = Json { ignoreUnknownKeys = true }
                val response = format.decodeFromString(AddUser.serializer(), result)
                response.authToken
            } else {
                null
            }
        }

        @JvmStatic fun getAllControllerInfo(ipAddr: String?, port: Int, token: String): String? {
            return HttpRequest(ipAddr, port, token, "", "GET", "")
        }

        @JvmStatic fun getGlobalOrientation(ipAddr: String?, port: Int, token: String): String? {
            return HttpRequest(ipAddr, port, token, "panelLayout/globalOrientation", "GET", "")
        }

        @JvmStatic fun getPanelsLayout(ipAddr: String?, port: Int, token: String): String? {
            return HttpRequest(ipAddr, port, token, "panelLayout/layout/", "GET", "")
        }

        @JvmStatic fun getEffect(ipAddr: String?, port: Int, token: String): String? {
            return HttpRequest(ipAddr, port, token, "effects/select", "GET", "")
        }

        @JvmStatic fun setBrightness(ipAddr: String?, port: Int, token: String, brightness: Int, onOff: Boolean): String? {
            return HttpRequest(
                ipAddr,
                port,
                token,
                "state",
                "PUT",
                String.format(Locale.getDefault(), "{\"brightness\" : {\"value\":%d, \"duration\":30}, \"on\" : {\"value\":%b}}", brightness, onOff)
            )
        }

        @JvmStatic fun setScene(ipAddr: String?, port: Int, token: String, scene: String?): String? {
            return HttpRequest(ipAddr, port, token, "effects", "PUT", String.format("{\"select\" : \"%s\"}", scene))
        }

        @JvmStatic fun setCCT(ipAddr: String?, port: Int, token: String, cct: Int, brightness: Int, onOff: Boolean): String? {
            return HttpRequest(
                ipAddr,
                port,
                token,
                "state",
                "PUT",
                String.format(Locale.getDefault(), "{\"ct\" : {\"value\": %d}, \"brightness\" : {\"value\":%d, \"duration\":30}, \"on\" : {\"value\":%b}}", cct, brightness, onOff)
            )
        }

        @JvmStatic fun setHSV(ipAddr: String?, port: Int, token: String, brightness: Int, hue: Int, sat: Int, onOff: Boolean): String? {
            return HttpRequest(
                ipAddr,
                port,
                token,
                "state",
                "PUT",
                String.format(
                    Locale.getDefault(),
                    "{\"hue\" : {\"value\":%d}, \"sat\" : {\"value\":%d} , \"brightness\" : {\"value\":%d, \"duration\":30}, \"on\" : {\"value\":%b} }",
                    hue,
                    sat,
                    brightness,
                    onOff
                )
            )
        }

        @JvmStatic fun enableStreamControlMode(ipAddr: String?, port: Int, token: String, version: Int): Int? {
            var body = "{\"write\": {\"extControlVersion\": \"v%d\",\"command\": \"display\",\"animType\": \"extControl\"}}"
            body = String.format(Locale.getDefault(), body, version)
            val result = HttpRequest(ipAddr, port, token, "effects", "PUT", body) ?: return null
            return if (version == 1) {
                val format = Json { ignoreUnknownKeys = true }
                val response = format.decodeFromString(Streaming.serializer(), result)
                response.streamControlPort
            } else {
                60222
            }
        }

        @JvmStatic fun sendColorStream(frames: List<ColorFramePanel>, version: Int, ipAddr: String?, port: Int) {
            val outputStream = ByteArrayOutputStream()
            if (version == 1) {
                outputStream.write(frames.size)
            } else {
                outputStream.write(frames.size shr 8 and 0xFF)
                outputStream.write(frames.size and 0xFF)
            }
            var i = 0
            val length = frames.size
            while (i < length) {
                if (version == 1) {
                    outputStream.write(frames[i].panelID)
                    outputStream.write(1)
                } else {
                    outputStream.write(frames[i].panelID shr 8 and 0xFF)
                    outputStream.write(frames[i].panelID and 0xFF)
                }
                outputStream.write(frames[i].colorR)
                outputStream.write(frames[i].colorG)
                outputStream.write(frames[i].colorB)
                outputStream.write(0)
                if (version == 1) {
                    outputStream.write(frames[i].transTime)
                } else {
                    outputStream.write(frames[i].transTime shr 8 and 0xFF)
                    outputStream.write(frames[i].transTime and 0xFF)
                }
                i++
            }
            val frameData = outputStream.toByteArray()
            if (frameData.isNotEmpty()) {
                try {
                    udpSocket.send(DatagramPacket(frameData, frameData.size, InetAddress.getByName(ipAddr), port))
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        @Serializable data class AddUser(@SerialName("auth_token") var authToken: String?)

        @Serializable data class Streaming(@SerialName("streamControlPort") var streamControlPort: Int?)
    }
}