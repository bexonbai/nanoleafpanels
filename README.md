AAR Download: https://bitbucket.org/bexonbai/nanoleafpanels/downloads/

layout.xml
```
<me.nanoleaf.plug.panel.view.PanelsView
    android:id="@+id/panel"
    android:layout_width="400dp"
    android:layout_height="200dp" />
```

Kotlin:

Network Thread
``` kotlin
var authorization = NanoleafOpenApi.authorization("10.0.0.141", 16021) // Device IP
var allControllerInfo = NanoleafOpenApi.getAllControllerInfo("10.0.0.141", 16021, authorization!!)
var panels = NanoleafPanels(allControllerInfo!!, this)
```
UI Thread
``` kotlin
var panelsView: PanelsView = findViewById(R.id.panel)
panelsView.setPanelsBitmap(panels.bitmap)
```